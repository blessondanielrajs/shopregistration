-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 04, 2020 at 03:28 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ranibroliers`
--

-- --------------------------------------------------------

--
-- Table structure for table `table1`
--

CREATE TABLE `table1` (
  `CUSTOMERID` int(10) NOT NULL,
  `NAME` varchar(50) NOT NULL,
  `ADDRESS` varchar(100) NOT NULL,
  `PHNO` int(11) NOT NULL,
  `EMAILID` varchar(30) NOT NULL,
  `GENDER` varchar(15) NOT NULL,
  `TYPEOFMEAT` varchar(20) NOT NULL,
  `WEIGHT` float NOT NULL,
  `AMOUNT` int(11) NOT NULL,
  `SIZE` varchar(10) NOT NULL,
  `DATEOFORDER` datetime(6) NOT NULL,
  `DATEOFDELIVERY` datetime(6) NOT NULL,
  `CASHTYPE` varchar(10) NOT NULL,
  `WHATSAPPNO` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table1`
--

INSERT INTO `table1` (`CUSTOMERID`, `NAME`, `ADDRESS`, `PHNO`, `EMAILID`, `GENDER`, `TYPEOFMEAT`, `WEIGHT`, `AMOUNT`, `SIZE`, `DATEOFORDER`, `DATEOFDELIVERY`, `CASHTYPE`, `WHATSAPPNO`) VALUES
(1, 'DANI', 'NATHAM', 123456, 'urldan323@gmail.com', 'MALE', 'BROILER', 2, 240, 'SMALL', '2020-12-12 12:33:03.000000', '2019-11-10 11:04:07.000000', 'PAYTM', NULL),
(2, 'PRAISON', 'MADURAI', 12345689, 'blesso@gmail.com', 'MALE', 'KADAI', 2, 360, 'SMALL', '2019-10-20 10:20:32.000000', '2019-12-21 12:32:02.000000', 'PHPE', NULL),
(3, 'RAJA', 'MADURAI', 1234568, 'raj@gmail.com', 'MALE', 'KADAI', 5, 360, 'BROLILER', '2019-08-20 09:20:32.000000', '2019-01-21 12:32:02.000000', 'PHPE', NULL),
(4, 'GLORY', 'MADURAI', 12689, 'glory@gmail.com', 'FEMALE', 'COUNTRY HEN', 8, 404, 'MEDIUM', '2019-10-20 01:20:32.000000', '2019-12-21 02:32:03.000000', 'PATYM', NULL),
(5, 'SELVI', 'DINDIGAL', 12349, 'selvi@gmail.com', 'FEMALE', 'KADAI', 2, 360, 'SMALL', '2018-10-20 10:20:32.000000', '2018-12-21 12:32:02.000000', 'PHPE', NULL),
(6, 'SAM', 'JAIPUR', 125689, 'sam@gmail.com', 'MALE', 'KADAI', 3, 460, 'LARGE', '2019-01-20 10:02:32.000000', '2019-01-21 12:32:02.000000', 'PHPE', NULL),
(7, 'FRANCIS', 'VEAMAS', 12349, 'francis@gmail.com', 'MALE', 'COUNTRY HEN', 1, 420, 'MEDIUM', '2018-10-20 10:20:32.000000', '2018-12-21 12:32:02.000000', 'PHPE', NULL),
(8, 'BEAULH', 'VEMAS', 345689, 'beaulh@gmail.com', 'FEMALE', 'BROILER', 2, 350, 'LARGE', '2017-10-20 10:20:32.000000', '2017-12-21 12:32:02.000000', 'COD', NULL),
(9, 'BLESS', 'KARUR', 125689, 'bless@gmail.com', 'MALE', 'KADAI', 5, 860, 'SMALL', '2018-10-20 10:20:32.000000', '2018-12-21 12:32:02.000000', 'COD', NULL),
(10, 'MARY', 'KARUR', 15689, 'mary@gmail.com', 'FEMALE', 'KADAI', 1, 310, 'LARGE', '2015-10-20 10:20:32.000000', '2015-12-21 12:32:02.000000', 'PATYM', NULL),
(11, 'SSS', 'DDDD', 123, 'FFH', 'Male', '400', 2.5, 400, 'Small', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 2334),
(12, 'Blesson daniel Raj', '1/431 kamaraj nagar\nNatham\nNatham', 2147483647, 'urldan324@gmail.com', 'Male', '160', 1, 160, 'Medium', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 2147483647),
(13, '   john blesswin', '1/431 kamaraj nagar\nNatham\nNatham', 2147483647, 'urldan324@gmail.com', 'Male', '160', 1, 160, 'Medium', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 2147483647),
(14, 'Blesson daniel Raj', '1/431 kamaraj nagar\r\nNatham\r\nNatham', 1234, 'dani@gmail.com', 'Male', '240', 1.5, 240, 'Medium', '2020-07-16 22:32:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 1234),
(15, 'Blesson daniel Raj', '1/431 kamaraj nagar\nNatham\nNatham', 123457, 'urldan324@gmail.com', 'Male', '240', 1.5, 240, 'Small', '2020-07-08 22:40:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 124566),
(16, 'Blesson daniel Raj', '1/431 kamaraj nagar\nNatham\nNatham', 123457, 'urldan324@gmail.com', 'Male', '240', 1.5, 240, 'Small', '2020-07-08 22:40:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 124566),
(17, 'jacob', '1/431 kamaraj nagar\nNatham\nNatham', 91981991, 'khkadddddddh', 'Female', '690', 1.5, 690, 'Small', '2020-06-02 22:42:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 1111119898),
(18, 'jacob', '1/431 kamaraj nagar\nNatham\nNatham', 91981991, 'khkadddddddh', 'Female', '690', 1.5, 690, 'Small', '2020-06-02 22:42:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 1111119898),
(19, '', '', 0, '', '', '', 0, 0, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', '', 0),
(20, 'jacob', '1/431 kamaraj nagar\nNatham\nNatham', 91981991, 'khkadddddddh', 'Female', '690', 1.5, 690, 'Small', '2020-06-02 22:42:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 1111119898),
(21, 'jacob', '1/431 kamaraj nagar\nNatham\nNatham', 91981991, 'khkadddddddh', 'Female', '690', 1.5, 690, 'Small', '2020-06-02 22:42:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 1111119898),
(22, 'jacob', '1/431 kamaraj nagar\nNatham\nNatham', 91981991, 'khkadddddddh', 'Female', '690', 1.5, 690, 'Small', '2020-06-02 22:42:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 1111119898),
(23, 'jacob', '1/431 kamaraj nagar\nNatham\nNatham', 91981991, 'khkadddddddh', 'Female', '690', 1.5, 690, 'Small', '2020-06-02 22:42:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 1111119898),
(24, 'jacob', '1/431 kamaraj nagar\nNatham\nNatham', 91981991, 'khkadddddddh', 'Female', '690', 1.5, 690, 'Small', '2020-06-02 22:42:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 1111119898),
(25, 'jacob', '1/431 kamaraj nagar\nNatham\nNatham', 91981991, 'khkadddddddh', 'Female', '690', 1.5, 690, 'Small', '2020-06-02 22:42:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 1111119898),
(26, 'sdads', 'jk', 0, 'jk', 'Male', '160', 1, 160, 'Small', '2020-07-03 23:53:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(27, 'sdads', 'jk', 0, 'jk', 'Male', '160', 1, 160, 'Small', '2020-07-03 23:53:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(28, 'sdads', 'jk', 0, 'jk', 'Male', '160', 1, 160, 'Small', '2020-07-03 23:53:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(29, 'sdads', 'jk', 0, 'jk', 'Male', '160', 1, 160, 'Small', '2020-07-03 23:53:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(30, 'sdads', 'jk', 0, 'jk', 'Male', '160', 1, 160, 'Small', '2020-07-03 23:53:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(31, 'sdads', 'jk', 0, 'jk', 'Male', '160', 1, 160, 'Small', '2020-07-03 23:53:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(32, 'sdads', 'jk', 0, 'jk', 'Male', '160', 1, 160, 'Small', '2020-07-03 23:53:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(33, 'sdads', 'jk', 0, 'jk', 'Male', '160', 1, 160, 'Small', '2020-07-03 23:53:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(34, 'sdads', 'jk', 0, 'jk', 'Male', '160', 1, 160, 'Small', '2020-07-03 23:53:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(35, 'sdads', 'jk', 0, 'jk', 'Male', '160', 1, 160, 'Small', '2020-07-03 23:53:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(36, 'j', 'j', 0, 'j', 'Male', '460', 1, 460, 'Small', '2020-07-01 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(37, 'j', 'j', 0, 'j', 'Male', '460', 1, 460, 'Small', '2020-07-01 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(38, '', '', 0, '', '', '', 0, 0, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', '', 0),
(39, '', '', 0, '', '', '', 0, 0, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', '', 0),
(40, '', '', 0, '', '', '', 0, 0, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(41, '11223', '', 0, '', 'Male', '', 0, 0, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(42, '11223', '', 0, '', 'Male', '', 0, 0, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(43, '', '', 0, '', '', '', 0, 0, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(44, '', '', 0, '', '', '', 0, 0, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(45, 'axx', '', 0, '', 'Male', '12', 0, 12, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(46, 'axx', '', 0, '', 'Male', '12', 0, 12, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(47, 'axx', '', 0, '', 'Male', '12', 0, 12, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(48, '', '', 0, '', '', '', 0, 0, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(49, '', '', 0, '', '', '', 0, 0, '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 0),
(50, 'kumar', 'natham', 123456789, 'urldanu324', 'Male', '240', 1.5, 240, 'Small', '2020-07-29 22:09:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 123456789),
(51, 'kumar', 'natham', 123456789, 'urldanu324', 'Male', '240', 1.5, 240, 'Small', '2020-07-29 22:09:00.000000', '0000-00-00 00:00:00.000000', 'CoD', 123456789),
(52, 'suresh', 'salem', 123456789, 'urldan', 'Male', '1610', 3.5, 1610, 'Medium', '2020-07-29 22:09:00.000000', '0000-00-00 00:00:00.000000', 'PhPe', 123456789),
(53, 'mani', 'madurai', 1234598, 'mani@gamil.com', 'Female', '140', 3.5, 140, 'Small', '2020-07-29 22:09:00.000000', '0000-00-00 00:00:00.000000', 'PhPe', 123456566),
(54, 'slevi', 'madurai', 1234598, 'selvi@gamil.com', 'Female', '800', 5, 800, 'Small', '2020-07-29 22:09:00.000000', '0000-00-00 00:00:00.000000', 'PhPe', 123456566);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `table1`
--
ALTER TABLE `table1`
  ADD PRIMARY KEY (`CUSTOMERID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `table1`
--
ALTER TABLE `table1`
  MODIFY `CUSTOMERID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
